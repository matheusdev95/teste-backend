import { Router } from 'express';
import SessionController from './app/controllers/SessionController';
import UserController from './app/controllers/UserController';
import AdminController from './app/controllers/AdminController';
import MovieController from './app/controllers/MovieController';
import authMiddleware from './app/middlewares/auth';
import adminMiddleware from './app/middlewares/admin';
const routes = new Router();

routes.get('/dashboard', (req, res) => {
  return res.json({ mesagede: 'Olá' });
});
routes.post('/users', UserController.store);
routes.post('/admins', AdminController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleware);
routes.put('/users/:id', UserController.update);
routes.put('/admins/:id', AdminController.update);
routes.delete('/users/:id', UserController.desactive);
routes.delete('/admins/:id', AdminController.desactive);
routes.patch('/movies/:id', MovieController.vote);
routes.get('/movies', MovieController.index);
routes.get('/movies/:id', MovieController.show);

routes.use(adminMiddleware);
routes.post('/movies', MovieController.store);

export default routes;
