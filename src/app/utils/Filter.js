const filterMap = {
  name: (filters, params) => {
    filters.name = params.name;
    return filters;
  },
  director: (filters, params) => {
    filters.director = params.director;
    return filters;
  },
  gender: (filters, params) => {
    filters.gender = params.gender;
    return filters;
  },
  actors: (filters, params) => {
    filters.actors = params.actors;
    return filters;
  },
};

class Filter {
  filter(params, filters) {
    for (let name in params) {
      if (filterMap.hasOwnProperty(name)) {
        filters = filterMap[name](filters, params);
      }
    }
  }

  getFilters(params, filters) {
    this.filter(params, filters);
    return filters;
  }
}

export default new Filter();
