import Admin from '../models/Admin';

export default async (req, res, next) => {
  if (
    await Admin.findOne({
      where: {
        user_id: req.userId,
      },
    })
  )
    return next();

  return res.status(401).json({
    error: 'Not Admin',
  });
};
