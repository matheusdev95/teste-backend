import Movie from '../models/Movie';
import Filter from '../utils/Filter';

class MovieController {
  async store(req, res) {
    try {
      const movie = await Movie.create(req.body);
      return res.status(201).send(movie);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async vote(req, res) {
    try {
      const movie = await Movie.findByPk(req.params.id);
      await movie.update({
        vote: ++movie.vote,
        note_total: (movie.note_total += req.body.note),
        vote_average: (movie.note_total += req.body.note) / ++movie.vote,
      });
      return res.status(200).send(movie);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async show(req, res) {
    try {
      const movie = await Movie.findByPk(req.params.id);
      return res.status(200).send(movie);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async index(req, res) {
    try {
      const filters = {};
      const movies = await Movie.findAll({
        where: Filter.getFilters(req.query, filters),
      });
      return res.status(200).send(movies);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }
}

export default new MovieController();
