import User from '../models/User';
import Admin from '../models/Admin';

class AdminController {
  async store(req, res) {
    try {
      if (
        await User.findOne({
          where: {
            email: req.body.email,
          },
        })
      )
        return res.status(400).json({
          error: 'Email is already in use!',
        });
      const user = await User.create(req.body);
      await Admin.create({ user_id: user.id });
      return res.status(201).send(user);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const admin = await User.findByPk(req.params.id);
      await admin.update(req.body);
      return res.status(200).send(admin);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async desactive(req, res) {
    try {
      const user = await User.findByPk(req.params.id);
      await user.update({ is_active: false });
      const admin = await Admin.findOne({
        where: {
          user_id: user.id,
        },
      });

      await admin.update({ is_active: false });
      return res.status(200).send('Admin Disabled!');
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }
}

export default new AdminController();
