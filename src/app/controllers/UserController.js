import User from '../models/User';

class UserController {
  async store(req, res) {
    try {
      if (
        await User.findOne({
          where: {
            email: req.body.email,
          },
        })
      )
        return res.status(400).json({
          error: 'Email is already in use!',
        });
      const user = await User.create(req.body);
      return res.status(201).send(user);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const user = await User.findByPk(req.params.id);
      await user.update(req.body);
      return res.status(200).send(user);
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  async desactive(req, res) {
    try {
      const user = await User.findByPk(req.params.id);
      await user.update({ is_active: false });
      return res.status(200).send('User Disabled!');
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }
}

export default new UserController();
