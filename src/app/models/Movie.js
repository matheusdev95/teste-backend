import Sequelize, { Model } from 'sequelize';

class Movie extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        director: Sequelize.STRING,
        gender: Sequelize.STRING,
        actors: Sequelize.STRING,
        vote: Sequelize.INTEGER,
        note_total: Sequelize.INTEGER,
        vote_average: Sequelize.DOUBLE,
      },
      {
        sequelize,
      },
    );

    return this;
  }
}

export default Movie;
