import Sequelize, { Model } from 'sequelize';

class Admin extends Model {
  static init(sequelize) {
    super.init(
      {
        is_active: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'user_id',
    });
  }
}

export default Admin;
